package ru.ilinovsg.tm;

public interface IFactorial {
    int getFactorial(int n);
}
