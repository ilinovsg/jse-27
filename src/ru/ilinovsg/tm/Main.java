package ru.ilinovsg.tm;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

public class Main {

    public static void main(String[] args) {
        Factorial factorial = new Factorial();
        InvocationHandler handler = new InvocationHandlerImpl(factorial);
        IFactorial proxy = (IFactorial) Proxy.newProxyInstance(factorial.getClass().getClassLoader(),
                            Factorial.class.getInterfaces(), handler);

        System.out.println(proxy.getFactorial(5));
        System.out.println(proxy.getFactorial(3));
        System.out.println(proxy.getFactorial(5));
    }
}
